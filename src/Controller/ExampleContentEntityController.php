<?php

namespace Drupal\example\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\example\Entity\ExampleContentEntityInterface;

/**
 * Class ExampleContentEntityController.
 *
 * Returns responses for Example content entity routes.
 *
 * @package Drupal\example\Controller
 */
class ExampleContentEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Example content entity  revision.
   *
   * @param int $example_content_entity_revision
   *   The Example content entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($example_content_entity_revision) {
    $example_content_entity = $this->entityManager()->getStorage('example_content_entity')->loadRevision($example_content_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('example_content_entity');

    return $view_builder->view($example_content_entity);
  }

  /**
   * Page title callback for a Example content entity  revision.
   *
   * @param int $example_content_entity_revision
   *   The Example content entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($example_content_entity_revision) {
    $example_content_entity = $this->entityManager()->getStorage('example_content_entity')->loadRevision($example_content_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $example_content_entity->label(), '%date' => format_date($example_content_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Example content entity .
   *
   * @param \Drupal\example\Entity\ExampleContentEntityInterface $example_content_entity
   *   A Example content entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ExampleContentEntityInterface $example_content_entity) {
    $account = $this->currentUser();
    $langcode = $example_content_entity->language()->getId();
    $langname = $example_content_entity->language()->getName();
    $languages = $example_content_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $example_content_entity_storage = $this->entityManager()->getStorage('example_content_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $example_content_entity->label()]) : $this->t('Revisions for %title', ['%title' => $example_content_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all example content entity revisions") || $account->hasPermission('administer example content entity entities')));
    $delete_permission = (($account->hasPermission("delete all example content entity revisions") || $account->hasPermission('administer example content entity entities')));

    $rows = [];

    $vids = $example_content_entity_storage->revisionIds($example_content_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\example\ExampleContentEntityInterface $revision */
      $revision = $example_content_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $example_content_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.example_content_entity.revision', ['example_content_entity' => $example_content_entity->id(), 'example_content_entity_revision' => $vid]));
        }
        else {
          $link = $example_content_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.example_content_entity.translation_revert', ['example_content_entity' => $example_content_entity->id(), 'example_content_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.example_content_entity.revision_revert', ['example_content_entity' => $example_content_entity->id(), 'example_content_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.example_content_entity.revision_delete', ['example_content_entity' => $example_content_entity->id(), 'example_content_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['example_content_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
