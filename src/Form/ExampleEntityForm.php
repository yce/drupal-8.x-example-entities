<?php

namespace Drupal\example\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\example\Entity\ExampleEntity;

/**
 * Class ExampleEntityForm.
 *
 * @package Drupal\example\Form
 */
class ExampleEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var ExampleEntity $example_entity */
    $example_entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $example_entity->label(),
      '#description' => $this->t("Label for the Example entity."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $example_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\example\Entity\ExampleEntity::load',
      ],
      '#disabled' => !$example_entity->isNew(),
    ];

    $form['color'] = [
      '#title' => $this->t('Color'),
      '#type' => 'select',
      '#options' => $example_entity->getColorOptions(),
      '#default_value' => $example_entity->getColor(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $example_entity = $this->entity;
    $status = $example_entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Example entity.', [
          '%label' => $example_entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Example entity.', [
          '%label' => $example_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($example_entity->toUrl('collection'));
  }

}
