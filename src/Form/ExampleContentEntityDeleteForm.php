<?php

namespace Drupal\example\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Example content entity entities.
 *
 * @ingroup example
 */
class ExampleContentEntityDeleteForm extends ContentEntityDeleteForm {


}
