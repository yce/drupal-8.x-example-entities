<?php

namespace Drupal\example;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\example\Entity\ExampleContentEntityInterface;

/**
 * Defines the storage handler class for Example content entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Example content entity entities.
 *
 * @ingroup example
 */
interface ExampleContentEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Example content entity revision IDs for a specific Example content entity.
   *
   * @param \Drupal\example\Entity\ExampleContentEntityInterface $entity
   *   The Example content entity entity.
   *
   * @return int[]
   *   Example content entity revision IDs (in ascending order).
   */
  public function revisionIds(ExampleContentEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Example content entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Example content entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\example\Entity\ExampleContentEntityInterface $entity
   *   The Example content entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ExampleContentEntityInterface $entity);

  /**
   * Unsets the language for all Example content entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
