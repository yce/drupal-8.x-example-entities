<?php

namespace Drupal\example\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Example content entity entities.
 *
 * @ingroup example
 */
interface ExampleContentEntityInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Example content entity name.
   *
   * @return string
   *   Name of the Example content entity.
   */
  public function getName();

  /**
   * Sets the Example content entity name.
   *
   * @param string $name
   *   The Example content entity name.
   *
   * @return \Drupal\example\Entity\ExampleContentEntityInterface
   *   The called Example content entity entity.
   */
  public function setName($name);

  /**
   * Gets the Example content entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Example content entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Example content entity creation timestamp.
   *
   * @param int $timestamp
   *   The Example content entity creation timestamp.
   *
   * @return \Drupal\example\Entity\ExampleContentEntityInterface
   *   The called Example content entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Example content entity published status indicator.
   *
   * Unpublished Example content entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Example content entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Example content entity.
   *
   * @param bool $published
   *   TRUE to set this Example content entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\example\Entity\ExampleContentEntityInterface
   *   The called Example content entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Example content entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Example content entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\example\Entity\ExampleContentEntityInterface
   *   The called Example content entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Example content entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Example content entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\example\Entity\ExampleContentEntityInterface
   *   The called Example content entity entity.
   */
  public function setRevisionUserId($uid);

}
