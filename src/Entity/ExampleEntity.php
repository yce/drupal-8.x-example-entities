<?php

namespace Drupal\example\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Example entity entity.
 *
 * @ConfigEntityType(
 *   id = "example_entity",
 *   label = @Translation("Example entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\example\ExampleEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\example\Form\ExampleEntityForm",
 *       "edit" = "Drupal\example\Form\ExampleEntityForm",
 *       "delete" = "Drupal\example\Form\ExampleEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\example\ExampleEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "example_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/example_entity/{example_entity}",
 *     "add-form" = "/admin/structure/example_entity/add",
 *     "edit-form" = "/admin/structure/example_entity/{example_entity}/edit",
 *     "delete-form" = "/admin/structure/example_entity/{example_entity}/delete",
 *     "collection" = "/admin/structure/example_entity",
 *   }
 * )
 */
class ExampleEntity extends ConfigEntityBase implements ExampleEntityInterface {
  /**
   * The Example entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Example entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * Color property.
   *
   * @var string
   */
  protected $color;

  /**
   * {@inheritdoc}
   */
  public function getColor() {
    return $this->color;
  }

  /**
   * {@inheritdoc}
   */
  public function setColor($color) {
    $this->color = $color;
  }

  /**
   * {@inheritdoc}
   */
  public function getColorOptions() {
    return [
      'red' => t('Red'),
      'green' => t('Green'),
      'blue' => t('Blue'),
    ];
  }
}
