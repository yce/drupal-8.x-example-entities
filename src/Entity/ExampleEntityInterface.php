<?php

namespace Drupal\example\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Example entity entities.
 */
interface ExampleEntityInterface extends ConfigEntityInterface {
  /**
   * Gets color.
   *
   * @return string
   *   Machine name of the color.
   */
  public function getColor();

  /**
   * Sets the color.
   *
   * @param string $color
   *   The machine name of the color.
   *
   * @return $this
   *   Returns the entity object.
   */
  public function setColor($color);

  /**
   * Gets all available colors keyed by their machine name.
   *
   * @return string[]
   *   List of colors.
   */
  public function getColorOptions();
}
