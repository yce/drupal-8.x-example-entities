<?php

namespace Drupal\example\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Example content entity entity.
 *
 * @ingroup example
 *
 * @ContentEntityType(
 *   id = "example_content_entity",
 *   label = @Translation("Example content entity"),
 *   handlers = {
 *     "storage" = "Drupal\example\ExampleContentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\example\ExampleContentEntityListBuilder",
 *     "views_data" = "Drupal\example\Entity\ExampleContentEntityViewsData",
 *     "translation" = "Drupal\example\ExampleContentEntityTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\example\Form\ExampleContentEntityForm",
 *       "add" = "Drupal\example\Form\ExampleContentEntityForm",
 *       "edit" = "Drupal\example\Form\ExampleContentEntityForm",
 *       "delete" = "Drupal\example\Form\ExampleContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\example\ExampleContentEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\example\ExampleContentEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "example_content_entity",
 *   data_table = "example_content_entity_field_data",
 *   revision_table = "example_content_entity_revision",
 *   revision_data_table = "example_content_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer example content entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/example_content_entity/{example_content_entity}",
 *     "add-form" = "/admin/content/example_content_entity/add",
 *     "edit-form" = "/admin/content/example_content_entity/{example_content_entity}/edit",
 *     "delete-form" = "/admin/content/example_content_entity/{example_content_entity}/delete",
 *     "version-history" = "/admin/content/example_content_entity/{example_content_entity}/revisions",
 *     "revision" = "/admin/content/example_content_entity/{example_content_entity}/revisions/{example_content_entity_revision}/view",
 *     "revision_revert" = "/admin/content/example_content_entity/{example_content_entity}/revisions/{example_content_entity_revision}/revert",
 *     "translation_revert" = "/admin/content/example_content_entity/{example_content_entity}/revisions/{example_content_entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/content/example_content_entity/{example_content_entity}/revisions/{example_content_entity_revision}/delete",
 *     "collection" = "/admin/content/example_content_entity",
 *   },
 *   field_ui_base_route = "example_content_entity.settings"
 * )
 */
class ExampleContentEntity extends RevisionableContentEntityBase implements ExampleContentEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the example_content_entity owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Example content entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Example content entity entity.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Example content entity is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    // #1 Create base field for entity.
    $fields['colors'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Favorite colors'))
      ->setCardinality(
        FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
      )
      ->setDescription(t('Your favorite colors.'))
      ->setRevisionable(TRUE)
      // #2 Field settings.
      ->setSetting('allowed_values', [
        'red' => t('Red'),
        'green' => t('Green'),
        'blue' => t('Blue'),
        'yellow' => t('Yellow'),
      ])
      // #3 View display options.
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 11,
      ])
      // #4 Form display options.
      ->setDisplayOptions('form', [
        'type' => 'option_select',
        'weight' => 11,
      ])
      // #5 Set form and view widget configurable.
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    return $fields;
  }

}
