<?php

namespace Drupal\example;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for example_content_entity.
 */
class ExampleContentEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
