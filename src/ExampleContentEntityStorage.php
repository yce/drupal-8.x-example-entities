<?php

namespace Drupal\example;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\example\Entity\ExampleContentEntityInterface;

/**
 * Defines the storage handler class for Example content entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Example content entity entities.
 *
 * @ingroup example
 */
class ExampleContentEntityStorage extends SqlContentEntityStorage implements ExampleContentEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ExampleContentEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {example_content_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {example_content_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ExampleContentEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {example_content_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('example_content_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
