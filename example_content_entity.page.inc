<?php

/**
 * @file
 * Contains example_content_entity.page.inc.
 *
 * Page callback for Example content entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Example content entity templates.
 *
 * Default template: example_content_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_example_content_entity(array &$variables) {
  // Fetch ExampleContentEntity Entity Object.
  $example_content_entity = $variables['elements']['#example_content_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
